/* COPYRIGHT (C) 2020 INDUCTIOHEAT, INC. ALL RIGHTS RESERVED.
 *
 * THIS SOURCE CODE DISTRIBUTION IS THE SOLE PROPERTY OF INDUCTOHEAT, INC.
 * ANY REPRODUCTION IN PART OR AS A WHOLE IS STRICTLY PROHIBITED WITHOUT THE
 * WRITTEN PERMISSION OF INDUCTOHEAT, INC.
 */

/**
 *  @Project PLi
 *  @file app_data.c
 *  @brief Non-Volatile Memory Definitions for iCOM app
 */

#ifndef MFG_DATA_H_
#define MFG_DATA_H_

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Includes
 */
#include <stdint.h>

/*
 * Global Typedefs and Enums
 */
typedef struct
{
    uint8_t ucHwPartNumber[16];
    uint8_t ucHwSerialNumber[16];
} xMfgData_t;

/*
 * Global Variables
 */
extern xMfgData_t xMfgData;

#endif /* MFG_DATA_H_ */

