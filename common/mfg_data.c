/* COPYRIGHT (C) 2020 INDUCTIOHEAT, INC. ALL RIGHTS RESERVED.
 *
 * THIS SOURCE CODE DISTRIBUTION IS THE SOLE PROPERTY OF INDUCTOHEAT, INC.
 * ANY REPRODUCTION IN PART OR AS A WHOLE IS STRICTLY PROHIBITED WITHOUT THE
 * WRITTEN PERMISSION OF INDUCTOHEAT, INC.
 */

/**
 *  @Project PLi
 *  @file app_data.c
 *  @brief Non-Volatile Memory Definitions for iCOM app
 */

/**
 *  @file mfg_info.c
 *  @brief Manufacturing Information Memory Definitions
 */

/*
 * Includes
 */
/* Application Includes */
#include <mfg_data.h>

/* Defaults */
#define mfg_dataHW_PART_NUMBER      "31053-001"
#define mfg_dataHW_SERIAL_NUMBER    "00000000"

/*
 * Manufacturing Information
 */
xMfgData_t xMfgData = {.ucHwPartNumber = { mfg_dataHW_PART_NUMBER },
                       .ucHwSerialNumber = { mfg_dataHW_SERIAL_NUMBER }};
#pragma DATA_SECTION(xMfgData,".mfg_data");
