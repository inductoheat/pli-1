/* COPYRIGHT (C) 2020 INDUCTIOHEAT, INC. ALL RIGHTS RESERVED.
 *
 * THIS SOURCE CODE DISTRIBUTION IS THE SOLE PROPERTY OF INDUCTOHEAT, INC.
 * ANY REPRODUCTION IN PART OR AS A WHOLE IS STRICTLY PROHIBITED WITHOUT THE
 * WRITTEN PERMISSION OF INDUCTOHEAT, INC.
 */

/**
 *  @Project PLi
 *  @file app_data.c
 *  @brief Non-Volatile Memory Definitions for iCOM app
 */

/*
 * Includes
 */
/* Application Includes */
 #include <app_data.h>

/* Defaults */
#define app_dataSW_PART_NUMBER  "31053-001-700"
#define app_dataSW_VERSION      1,1,0,0

/*
 * KAM Data Table
 */
xAppData_t xAppData;
#pragma DATA_SECTION(xAppData,".kam");

/* Software Part number */
const char cSwPartNumber[16] = { app_dataSW_PART_NUMBER };

/* Software Version */
const char cSwVersion[4] = { app_dataSW_VERSION };
