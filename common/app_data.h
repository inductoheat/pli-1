/* COPYRIGHT (C) 2020 INDUCTIOHEAT, INC. ALL RIGHTS RESERVED.
 *
 * THIS SOURCE CODE DISTRIBUTION IS THE SOLE PROPERTY OF INDUCTOHEAT, INC.
 * ANY REPRODUCTION IN PART OR AS A WHOLE IS STRICTLY PROHIBITED WITHOUT THE
 * WRITTEN PERMISSION OF INDUCTOHEAT, INC.
 */

/**
 *  @project PLi
 *  @file app_data.h
 *  @brief Non-Volatile Memory Definitions for iCOM app
 */

#ifndef APP_DATA_H_
#define APP_DATA_H_

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Includes
 */
#include <stdint.h>
#include <time.h>
#include "common.h"

/*
 * Global Typedefs and Enums
 */

/* <PID config mode records */

typedef struct
{
    uint32_t usPidFlags;
    float fpFrequencyMax;
    float fpFrequencyMin;
    float fpPidKc;
    float fpPidTi;
    float fpPidTd;
    float fpPidDelta;
    float fpPidRamprate;
    float fpPidKcTable[101];
    float fpIlimTable[101];
} xPidConfig_t;

/**< App data file */
/* NOTE: M3 requires 8-byte alignment for int64's */
typedef struct
{
    uint32_t ulCrc;
    uint32_t ulRsvd;
    time_t xCalTimestamp;
    uint32_t ulCalPartNumber[4];
    uint32_t ulCalVersion;
    uint32_t ulSystemStatus;
    uint32_t ulSystemControl;
    uint16_t usAdcRef;
    uint16_t usDcBusSamples;
    uint16_t usIdcSamples;
    uint16_t usRmsSamples;
    float fpDcBusMaxVoltage;
    float fpDcBusMinVoltage;
    float fpIdcMaxVoltage;
    float fpIdcMinVoltage;
    float fpRmsMaxVoltage;
    float fpRmsMinVoltage;
    xPidConfig_t xRecord[eREG_COUNT];
} xAppData_t;

/* Global Macros and Constants */
/* System Control Flags */
#define app_dataCONTROL_NONE                (0)

/* System Status Flags */
#define app_dataSTATUS_NONE                 (0)

/* PID Control Flags */
#define app_dataPID_FLAGS_NONE              (0)
#define app_dataPID_FLAGS_KC_TABLE_ENABLE   (1)

/*
 * Global Variables
 */
extern xAppData_t xAppData;
extern const char cSwPartNumber[16];
extern const char cSwVersion[4];

#endif /* APP_DATA_H_ */
