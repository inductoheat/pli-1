/* COPYRIGHT (C) 2020 INDUCTIOHEAT, INC. ALL RIGHTS RESERVED.
 *
 * THIS SOURCE CODE DISTRIBUTION IS THE SOLE PROPERTY OF INDUCTOHEAT, INC.
 * ANY REPRODUCTION IN PART OR AS A WHOLE IS STRICTLY PROHIBITED WITHOUT THE
 * WRITTEN PERMISSION OF INDUCTOHEAT, INC.
 */

/**
 *  @Project PLi
 *  @file app_data.c
 *  @brief Non-Volatile Memory Definitions for iCOM app
 */

#ifndef APP_LOG_H_
#define APP_LOG_H_

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Includes
 */
/* Standard Includes */
#include <stdarg.h>

/* TI-RTOS Includes */
#include <xdc/runtime/System.h>

/*
 * Global Macros and Constants
 */
#define app_logEOL "\r\n"

#define LOG_INFO(...) do {      \
    System_printf("INFO\t");    \
    System_printf(__VA_ARGS__); \
    System_printf(app_logEOL);         \
} while(0)

#define LOG_DATA(...) do {      \
    System_printf("DATA\t");    \
    System_printf(__VA_ARGS__); \
    System_printf(app_logEOL);         \
} while(0)

#define LOG_ERROR(...) do {     \
    System_printf("ERROR\t");   \
    System_printf("%s:%d\t", __FUNCTION__, __LINE__); \
    System_printf(__VA_ARGS__); \
    System_printf(app_logEOL);         \
} while(0)

#endif /* APP_LOG_H_ */
