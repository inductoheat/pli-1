#ifndef _INTEGER

typedef signed int INT;
typedef unsigned int UINT;

/* These types are assumed as 8-bit integer */
typedef signed char CHAR;
typedef unsigned char UCHAR;
typedef unsigned char BYTE;

/* These types are assumed as 16-bit integer */
typedef signed short SHORT;
typedef unsigned short USHORT;
typedef unsigned short WORD;

/* These types are assumed as 32-bit integer */
typedef signed long LONG;
typedef unsigned long ULONG;
typedef unsigned long DWORD;

/* Boolean type */
#define BOOL           unsigned char
#define FALSE          0
#define TRUE           1
//typedef enum { FALSEc = 0, TRUEc }BOOLc;


#define _INTEGER
#endif


