//###########################################################################
// FILE:   set_pinout_f28m35x.h
// TITLE:  GPIO setup for the F28M35x controlCARD.
//###########################################################################
// $TI Release: F28M35x Support Library v201 $
// $Release Date: Fri Jun  7 10:51:13 CDT 2013 $
//###########################################################################

#ifndef __SET_PINOUT_H__
#define __SET_PINOUT_H__

//*****************************************************************************
// SDCard SSI port
//*****************************************************************************
#define SDC_SSI_BASE            SSI0_BASE
#define SDC_SSI_SYSCTL_PERIPH   SYSCTL_PERIPH_SSI0

//*****************************************************************************
// GPIO for SDCard SSI pins
//*****************************************************************************
#define SDC_GPIO_PORT_BASE      GPIO_PORTD_BASE
#define SDC_GPIO_SYSCTL_PERIPH  SYSCTL_PERIPH_GPIOD
#define SDC_SSI_CLK             GPIO_PIN_2
#define SDC_SSI_TX              GPIO_PIN_0
#define SDC_SSI_RX              GPIO_PIN_1

#define SDC_SSI_PINS            (SDC_SSI_TX | SDC_SSI_RX | SDC_SSI_CLK)

//*****************************************************************************
// GPIO for the SDCard chip select
//*****************************************************************************
#define SDCARD_CS_PERIPH   SYSCTL_PERIPH_GPIOD
#define SDCARD_CS_BASE     GPIO_PORTD_BASE
#define SDCARD_CS_PIN      GPIO_PIN_3

//*****************************************************************************
// GPIO for the user LEDs
//*****************************************************************************
#define LED_0_PERIPH       SYSCTL_PERIPH_GPIOC
#define LED_0_BASE         GPIO_PORTC_BASE
#define LED_0_PIN          GPIO_PIN_6

#define LED_1_PERIPH       SYSCTL_PERIPH_GPIOC
#define LED_1_BASE         GPIO_PORTC_BASE
#define LED_1_PIN          GPIO_PIN_7



//*****************************************************************************
//
// Public function prototypes.
//
//*****************************************************************************
extern void PinoutSet(void);

#endif // __SET_PINOUT_H__
