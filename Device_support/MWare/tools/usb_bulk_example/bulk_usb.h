//###########################################################################
// FILE:   bulk_usb.h
// TITLE:  Public header for the USB interface used by the usb_bulk_example
//         application.
//###########################################################################
// $TI Release: F28M35x Support Library v201 $
// $Release Date: Fri Jun  7 10:51:13 CDT 2013 $
//###########################################################################
#ifndef __BULK_USB_H__
#define __BULK_USB_H__

extern BOOL InitializeDevice(void);
extern BOOL TerminateDevice(void);
extern BOOL WriteUSBPacket(unsigned char *pcBuffer, unsigned long ulSize,
                           unsigned long *pulWritten);
extern BOOL ReadUSBPacket(unsigned char *pcBuffer, unsigned long ulSize,
                          unsigned long *pulRead);

#endif


