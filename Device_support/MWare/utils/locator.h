//###########################################################################
// FILE:   locator.h
// TITLE:  Prototypes for the device locator server.
//###########################################################################
// $TI Release: F28M35x Support Library v201 $
// $Release Date: Fri Jun  7 10:51:13 CDT 2013 $
//###########################################################################

#ifndef __LOCATOR_H__
#define __LOCATOR_H__

//*****************************************************************************
// If building with a C++ compiler, make all of the definitions in this header
// have a C binding.
//*****************************************************************************
#ifdef __cplusplus
extern "C"
{
#endif

//*****************************************************************************
// Function prototypes.
//*****************************************************************************
extern void LocatorInit(void);
extern void LocatorBoardTypeSet(unsigned long ulType);
extern void LocatorBoardIDSet(unsigned long ulID);
extern void LocatorClientIPSet(unsigned long ulIP);
extern void LocatorMACAddrSet(unsigned char *pucMACArray);
extern void LocatorVersionSet(unsigned long ulVersion);
extern void LocatorAppTitleSet(const char *pcAppTitle);

//*****************************************************************************
// Mark the end of the C bindings section for C++ compilers.
//*****************************************************************************
#ifdef __cplusplus
}
#endif

#endif // __LOCATOR_H__


