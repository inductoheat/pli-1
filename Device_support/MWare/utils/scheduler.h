//###########################################################################
// FILE:   scheduler.h
// TITLE:  Public header for the simple timed function scheduler module.
//###########################################################################
// $TI Release: F28M35x Support Library v201 $
// $Release Date: Fri Jun  7 10:51:13 CDT 2013 $
//###########################################################################

#ifndef __SCHEDULER_H__
#define __SCHEDULER_H__

//*****************************************************************************
// If building with a C++ compiler, make all of the definitions in this header
// have a C binding.
//*****************************************************************************
#ifdef __cplusplus
extern "C"
{
#endif

//*****************************************************************************
//! \addtogroup scheduler_api
//! @{
//*****************************************************************************

//*****************************************************************************
// Prototype of a function that the scheduler can call periodically.
//*****************************************************************************
typedef void (*tSchedulerFunction)(void *pvParam);

//*****************************************************************************
//! The structure defining a function which the scheduler will call
//! periodically.
//*****************************************************************************
typedef struct
{
        //! A pointer to the function which is to be called periodically by the
        //! scheduler.
        void (*pfnFunction)(void *);

        //! The parameter which is to be passed to this function when it is
        // called.
        void *pvParam;

        //! The frequency the function is to be called expressed in terms of
        //! system ticks.  If this value is 0, the function will be called on
	//! every call to SchedulerRun.
        unsigned long ulFrequencyTicks;

        //! Tick count when this function was last called.  This field is
        //! updated by the scheduler.
        unsigned long ulLastCall;

        //! A flag indicating whether or not this task is active.  If true, the
        //! function will be called periodically.  If false, the function is
        //! disabled and will not be called.
        tBoolean bActive;
}
tSchedulerTask;

//*****************************************************************************
//! This global table must be populated by the client and contains information
//! on each function that the scheduler is to call.
//*****************************************************************************
extern tSchedulerTask g_psSchedulerTable[];

//*****************************************************************************
//! This global variable must be exported by the client.  It must contain the
//! number of entries in the g_psSchedulerTable array.
//*****************************************************************************
extern unsigned long g_ulSchedulerNumTasks;

//*****************************************************************************
// Close the Doxygen group.
//! @}
//*****************************************************************************

//*****************************************************************************
// Public function prototypes
//*****************************************************************************
extern void SchedulerSysTickIntHandler(void);
extern void SchedulerInit(unsigned long ulTicksPerSecond);
extern void SchedulerRun(void);
extern void SchedulerTaskEnable(unsigned long ulIndex, tBoolean bRunNow);
extern void SchedulerTaskDisable(unsigned long ulIndex);
extern unsigned long SchedulerTickCountGet(void);
extern unsigned long SchedulerElapsedTicksGet(unsigned long ulTickCount);
extern unsigned long SchedulerElapsedTicksCalc(unsigned long ulTickStart,
                                               unsigned long ulTickEnd);

//*****************************************************************************
// Mark the end of the C bindings section for C++ compilers.
//*****************************************************************************
#ifdef __cplusplus
}
#endif

#endif // __ SCHEDULER_H_


