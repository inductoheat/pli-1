//###########################################################################
// FILE:   ptpdlib.c
// TITLE:  ptpd Library Abstraction Layer.
//###########################################################################
// $TI Release: F28M35x Support Library v201 $
// $Release Date: Fri Jun  7 10:51:13 CDT 2013 $
//###########################################################################

//*****************************************************************************
// Include the library source code header files next.
//*****************************************************************************
#include "utils/ptpdlib.h"

//*****************************************************************************
// Include ptpd library code.
//*****************************************************************************
#include "ptpd-1rc1/src/arith.c"
#include "ptpd-1rc1/src/bmc.c"
#include "ptpd-1rc1/src/protocol.c"

//*****************************************************************************
// Include ptpd porting layer code.
//*****************************************************************************
#include "ptpd-1rc1/src/dep-lmi/ptpd_timer.c"
#include "ptpd-1rc1/src/dep-lmi/ptpd_servo.c"
#include "ptpd-1rc1/src/dep-lmi/ptpd_msg.c"
#include "ptpd-1rc1/src/dep-lmi/ptpd_net.c"


