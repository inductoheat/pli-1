;//###########################################################################
;// FILE:  F28M35x_DBGIER.asm
;// TITLE: Set the DBGIER register
;// DESCRIPTION:
;//  
;//  Function to set the DBGIER register (for realtime emulation).
;//  Function Prototype: void SetDBGIER(Uint16)
;//  Useage: SetDBGIER(value);
;//  Input Parameters: Uint16 value = value to put in DBGIER register. 
;//  Return Value: none          
;//###########################################################################
;// $TI Release: F28M35x Support Library v201 $ 
;// $Release Date: Fri Jun  7 10:51:13 CDT 2013 $ 
;//###########################################################################    
        .global _SetDBGIER
        .text
        
_SetDBGIER:
        MOV     *SP++,AL
        POP     DBGIER
        LRETR
        


