//###########################################################################
// FILE:   F28M35x_hwbist.c
// TITLE:  C28 Driver for the HWBIST module.
//###########################################################################
// $TI Release: F28M35x Support Library v201 $
// $Release Date: Fri Jun  7 10:51:13 CDT 2013 $
//###########################################################################

//*****************************************************************************
//! \addtogroup hwbist_api_c28 C28 HWBIST API Drivers
//! @{
//*****************************************************************************

#include "F28M35x_Device.h"
#include "F28M35x_HWBist_defines.h"
//*****************************************************************************
//
//! \internal
//! Block of NOPs.
//
//*****************************************************************************
#define NOP_SEQUENCE() \
    __asm(" NOP");    \
    __asm(" NOP");    \
    __asm(" NOP");    \
    __asm(" NOP");    \
    __asm(" NOP");    \
    __asm(" NOP");    \
    __asm(" NOP");    \
    __asm(" NOP");    \
    __asm(" NOP");    \
    __asm(" NOP");    \
    __asm(" NOP");    \
    __asm(" NOP");    \
    __asm(" NOP");    \
    __asm(" NOP");    \
    __asm(" NOP");    \
    __asm(" NOP");    \
    __asm(" NOP");    \
    __asm(" NOP");    \
    __asm(" NOP");    \
    __asm(" NOP");    \
    __asm(" NOP");    \
    __asm(" NOP");    \
    __asm(" NOP");    \
    __asm(" NOP");    \
    __asm(" NOP");    \
    __asm(" NOP");    \
    __asm(" NOP");    \
    __asm(" NOP");    \
    __asm(" NOP");    \
    __asm(" NOP");    \
    __asm(" NOP");    \
    __asm(" NOP");    \
    __asm(" NOP");    \
    __asm(" NOP");    \
    __asm(" NOP");    \
    __asm(" NOP");    \
    __asm(" NOP");    \
    __asm(" NOP");    \
    __asm(" NOP");    \
    __asm(" NOP");    \
    __asm(" NOP");

//////*****************************************************************************
//////
////// Always locate the NMI handler in RAM
//////
//////*****************************************************************************
//#pragma CODE_SECTION(F28M35x_HWBist_NmiHandler, "ramfuncs");

//*****************************************************************************
//
// Prototypes for the assembly functions located in F28M35x_hwbistcontext.s
//
//*****************************************************************************
extern void HWBISTASMSelfTestRun(void);          // C-callable HWBIST Assembly
                                                // Start Function
extern void HWBISTASMResetHandler(void);         // Address of HWBIST Reset
                                                // Routine (not C-callable)

//*****************************************************************************
//! Initializes the HWBIST module with default values.
//!
//! \param ulMicroInterval specifies the test micro interval. Valid values are
//!        40 - 47000 for Concerto F28M35x devices.
//! \param ulCoverage specifies the test coverage. Valid values are
//!        HWBIST_CONFIG_COV_95PERCENT and HWBIST_CONFIG_COV_99PERCENT.
//! \param ucShiftCLock specifies the scan chain shift clock divisor.
//!
//! \return None.
//*****************************************************************************
void HWBISTSelfTestInit(unsigned long ulMicroInterval,
                       unsigned long ulCoverage,
                       unsigned char ucShiftClock)
{
    EALLOW;

    NOP_SEQUENCE();

    // Test micro interval
    HWBistRegs.CSTCGCR1     = ulMicroInterval;

    // Macro interval (always 1 for Concerto devices)
    HWBistRegs.CSTCGCR2.all = 0x00000001;

    // Coverage
    HWBistRegs.CSTCGCR6.all = ulCoverage;

    // Restart test
    HWBistRegs.CSTCGCR5.all = 0x0000000a;

    // CSTCGCR7 configuration 
    HWBistRegs.CSTCGCR7.all = 0x00000010;

    // CSTCGCR8 configuration 
    HWBistRegs.CSTCGCR8.all = 0x00000032;

    // CSTCPCNT configuration 
    HWBistRegs.CSTCPCNT.all = 0x09c40514;

    // CSTCSADDR configuration 
    HWBistRegs.CSTCSADDR.all = 0x4f4c0000;

    NOP_SEQUENCE();

    // Configuration complete
    HWBistRegs.CSTCCONFIG.all = 0x0000000a;

    // Return address after HWBIST reset (address of assembly routine)
    HWBistRegs.CSTCRET = (unsigned long)&HWBISTASMResetHandler;

    HWBISTShiftClockSet(ucShiftClock);

    EDIS;
}

//*****************************************************************************
//! Starts a HWBIST self-test run.  This will run one micro-interval.
//!
//! \return true on test completion, false otherwise.
//*****************************************************************************
int HWBISTSelfTestMicroRun(void)
{
    // Run the self-test assembly routine
    // A HWBIST self-test micro interval will run and return back here
    // after a HWBIST triggered CPU core reset
    HWBISTASMSelfTestRun();

    // Check for HWBIST test completion flags
    if(HWBistRegs.CSTCGSTAT.bit.BISTDONE)
    {
        // Finished HWBIST test run cycle
        // Call HWBISTResultGet() to retrieve the test result
        return(1);
    }

    // HWBIST test not complete
    // The user should continue to call this function until it returns 1
    return(0);
}

//*****************************************************************************
//! Runs a complete HWBIST test.  This function runs until the HWBIST test cycle
//! completes with a status value.  For ease of use, this function also
//! returns a pass or fail value indicating the pass or fail state of the
//! HWBIST test.
//!
//! \return true if the CPU logic passed MISR checks, false if the CPU logic
//!         failed MISR checks.
//*****************************************************************************
int HWBISTSelfTestFullRun(void)
{
    // Run back-to-back micro tests until test completes
    while(!HWBISTSelfTestMicroRun());

    // Return simple (true/false) test result
    return HWBISTResultBasicGet();
}

//*****************************************************************************
//! Restarts the HWBIST test state.
//!
//! \return None.
//*****************************************************************************
void HWBISTSelfTestRestart(void)
{
    EALLOW;
    HWBistRegs.CSTCGCR5.bit.RESTART = 0xA;      // RESTART
    EDIS;
}

//*****************************************************************************
//! Injects a logic error to test the HWBIST module.
//!
//! \param ulLogicError is used to inject a specific logic error. Set this
//!        to a non-zero number to inject a logic error.  Set to zero to
//!        disable logic error injection.
//!
//! \return None.
//*****************************************************************************
void HWBISTTestLogicSet(unsigned long ulLogicError)
{
    EALLOW;
    HWBistRegs.CSTCTEST.all |= (ulLogicError & 0xFFFFF) << 12;
    EDIS;
}

//*****************************************************************************
//! Injects a NMI error to test the HWBIST module.
//!
//! \param bEnable is used to set the NMI error mode.  Set this to true to
//!        inject a NMI error, set to false to disable NMI error injection.
//!
//! \return None.
//*****************************************************************************
void HWBISTTestNmiSet(int bEnable)
{
    EALLOW;

    if(bEnable)
    {
        HWBistRegs.CSTCTEST.bit.TESTNMI = 0xA;
    }
    else
    {
        HWBistRegs.CSTCTEST.bit.TESTNMI = 0x0;
    }

    EDIS;
}

//*****************************************************************************
//! Injects a timeout error to test the HWBIST module.
//!
//! \param bEnable is used to set the timeout error mode.  Set this to true to
//!        inject a timeout error, set to false to disable NMI error injection.
//!
//! \return None.
//*****************************************************************************
void HWBISTTestTimeoutSet(int bEnable)
{
    EALLOW;

    if(bEnable)
    {
        HWBistRegs.CSTCTEST.bit.TESTTO = 0xA;
    }
    else
    {
        HWBistRegs.CSTCTEST.bit.TESTTO = 0x0;
    }

    EDIS;
}

//*****************************************************************************
//! Sets the HWBIST scan chain shift clock divider.
//!
//! \param ucShiftClock is the shift clock divider to use.  Valid values are
//!        HWBIST_CONFIG_SHIFTCLOCKDIV_1, HWBIST_CONFIG_SHIFTCLOCKDIV_2, and
//!        HWBIST_CONFIG_SHIFTCLOCKDIV_4.
//!
//! \return None.
//*****************************************************************************
void HWBISTShiftClockSet(unsigned char ucShiftClock)
{
    EALLOW;
    HWBistRegs.CSTCGCR7.bit.SCO = ucShiftClock & 0x3;
    EDIS;
}

//*****************************************************************************
//! Gets the HWBIST controller status register value.
//!
//! \return The HWBIST status register value.  To interpret the values, check
//!         the individual bits in the returned value.  Valid bit field values
//!         are HWBIST_RESULT_BISTDONE, HWBIST_RESULT_BISTDONE,
//!         HWBIST_RESULT_BISTFAIL, HWBIST_RESULT_INTCMPF, and
//!         HWBIST_RESULT_TOFAIL.
//*****************************************************************************
unsigned long HWBISTResultGet(void)
{
    return HWBistRegs.CSTCGSTAT.all;
}

//*****************************************************************************
//! Gets the basic status of the HWBIST test.  This function interprets the
//! status flags and MISR value and returns a simple pass or fail indicating
//! if the CPU core passed or failed the test.
//!
//! \return true if the CPU logic passed MISR checks, false if the CPU logic
//!         failed MISR checks.
//*****************************************************************************
int HWBISTResultBasicGet(void)
{
    unsigned long ulBistResult = HWBISTResultGet();

    if(((ulBistResult & HWBIST_RESULT_BISTDONE) > 0) &&
       ((ulBistResult & HWBIST_RESULT_TOFAIL) == 0) &&
       ((ulBistResult & HWBIST_RESULT_INTCMPF) == 0) &&
       ((ulBistResult & HWBIST_RESULT_BISTFAIL) == 0) &&
       ((ulBistResult & HWBIST_RESULT_NMI) == 0))
    {
        // BIST test flags show that the test controller
        // did not detect any errors, but we still have to
        // check the MISR value
        if(HWBISTCompareMisr(HWBISTMisrGet(), HWBISTMisrGoldenGet()))
        {
            // MISR comparison passed
            return(1);
        }
    }

    // Test not complete or HWBIST failure
    return(0);
}

//*****************************************************************************
//! Gets the HWBIST test MISR value
//!
//! \return The MISR value.  Note that the value is returned as a tMisr type.
//!         Use the HWBISTCompareMisr(...) function to compare tMisr types.
//*****************************************************************************
tMisr HWBISTMisrGet(void)
{
    tMisr mResult;
    mResult.Misr0 = HWBistRegs.CSTCMISR0;
    mResult.Misr1 = HWBistRegs.CSTCMISR1;
    mResult.Misr2 = HWBistRegs.CSTCMISR2;
    mResult.Misr3 = HWBistRegs.CSTCMISR3;
    return(mResult);
}

//*****************************************************************************
//! Gets the HWBIST Golden MISR value from ROM.  This function returns either
//! the 95% coverage value or the 99% coverage value depending on the
//! selected test coverage.
//!
//! \return The MISR value.  Note that the value is returned as a tMisr type.
//!         Use the HWBISTCompareMisr(...) function to compare tMisr types.
//*****************************************************************************
tMisr HWBISTMisrGoldenGet(void)
{
    tMisr mResult;

    // Read golden MISR from ROM based on current configuration
    switch(HWBistRegs.CSTCGCR6.bit.COV)
    {
        case HWBIST_CONFIG_COV_95PERCENT:
            mResult.Misr0 = *(unsigned long*)(HWBIST_95PERCENT_MISR0);
            mResult.Misr1 = *(unsigned long*)(HWBIST_95PERCENT_MISR1);
            mResult.Misr2 = *(unsigned long*)(HWBIST_95PERCENT_MISR2);
            mResult.Misr3 = *(unsigned long*)(HWBIST_95PERCENT_MISR3);
            break;
        case HWBIST_CONFIG_COV_99PERCENT:
            mResult.Misr0 = *(unsigned long*)(HWBIST_99PERCENT_MISR0);
            mResult.Misr1 = *(unsigned long*)(HWBIST_99PERCENT_MISR1);
            mResult.Misr2 = *(unsigned long*)(HWBIST_99PERCENT_MISR2);
            mResult.Misr3 = *(unsigned long*)(HWBIST_99PERCENT_MISR3);
            break;
        default:
            // Unrecognized value
            mResult.Misr0 = 0;
            mResult.Misr1 = 0;
            mResult.Misr2 = 0;
            mResult.Misr3 = 0;
            break;
    }

    return(mResult);
}

//*****************************************************************************
//! Compares two MISR values for equality.
//!
//! \param m1 The first MISR value.
//! \param m2 The second MISR value.
//!
//! \return true if the MISR values match, false otherwise.
//*****************************************************************************
int HWBISTCompareMisr(tMisr m1, tMisr m2)
{
    if((m1.Misr0 == m2.Misr0) &&
       (m1.Misr1 == m2.Misr1) &&
       (m1.Misr2 == m2.Misr2) &&
       (m1.Misr3 == m2.Misr3))
    {

        return(1);
    }
    else
    {
        return(0);
    }
}

//*****************************************************************************
// Close the Doxygen group.
//! @}
//*****************************************************************************
