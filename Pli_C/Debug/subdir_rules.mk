################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Each subdirectory must supply rules for building sources it contributes
build-1376584879:
	@$(MAKE) --no-print-directory -Onone -f subdir_rules.mk build-1376584879-inproc

build-1376584879-inproc: ../app.cfg
	@echo 'Building file: "$<"'
	@echo 'Invoking: XDCtools'
	"C:/ti/xdctools_3_23_02_47/xs" --xdcpath="C:/ti/bios_6_33_04_39/packages;C:/ti/ti-RTOS/tirtos_c2000_2_16_01_14/packages;C:/ti/ti-RTOS/tirtos_c2000_2_16_01_14/products/tidrivers_c2000_2_16_01_13/packages;C:/ti/ti-RTOS/tirtos_c2000_2_16_01_14/products/bios_6_45_02_31/packages;C:/ti/ti-RTOS/tirtos_c2000_2_16_01_14/products/ndk_2_25_00_09/packages;C:/ti/ti-RTOS/tirtos_c2000_2_16_01_14/products/uia_2_00_05_50/packages;" xdc.tools.configuro -o configPkg -t ti.targets.C28_float -p ti.platforms.concertoC28:F28M35H52C1 -r release -c "C:/ti/ccs1010/ccs/tools/compiler/ti-cgt-c2000_20.2.2.LTS" --compileOptions "-g --optimize_with_debug" "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

configPkg/linker.cmd: build-1376584879 ../app.cfg
configPkg/compiler.opt: build-1376584879
configPkg/: build-1376584879

%.obj: ../%.c $(GEN_OPTS) | $(GEN_FILES) $(GEN_MISC_FILES)
	@echo 'Building file: "$<"'
	@echo 'Invoking: C2000 Compiler'
	"C:/ti/ccs1010/ccs/tools/compiler/ti-cgt-c2000_20.2.2.LTS/bin/cl2000" -v28 -ml -mt --float_support=fpu32 --vcu_support=vcu0 --include_path="C:/Users/hdesai/Desktop/WORKSPACE/PLi/Device_support" --include_path="C:/Users/hdesai/Desktop/WORKSPACE/PLi/Pli_C" --include_path="C:/ti/ccs1010/ccs/tools/compiler/ti-cgt-c2000_20.2.2.LTS/include" -g --diag_warning=225 --diag_wrap=off --display_error_number --abi=coffabi --preproc_with_compile --preproc_dependency="$(basename $(<F)).d_raw" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '


